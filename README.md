# HotelAvatarProject

Skrypt rozmowy

    rezerwacje (pokoje, dostępnośc warunki)
    opcje dodatkowe (sauny, spa, atrakcje)
    zasady funkcjonowania hotelu (godziny zameldowania, wymeldowania, gdzie śniadanie, kiedy restauracja czynna)
    ocena (co było ok, co było źle, ogólne odczucie)

    Jakie są dostępne opcje wyżywienia?

W ofercie posiadamy dwie opcje wyżywienia: pobyt ze śniadaniem oraz pobyt ze śniadaniem i obiadokolacją.

    Jak mogę zarezerwować pokój?

Aby zarezerwować pokój można skontaktować się z recepcją telefonicznie, odwiedzić naszą stronę internetową lub wpisać poniżej datę pobytu i liczbę osób (Dzieci powyżej 13 lat sa liczone jako osoby dorosłe)

    Chciałbym zarezerwować pobyt XX.XX -XX.XX, 2 osoby.

Dziękujemy za zainteresowanie Naszą ofertą. Rezerwacja została zgłoszona na recepcje, szukamy już najlepszych wariantów. W czasie oczekiwania zachęcamy do zapoznania się z zasadami funkcjonowania hotelu. -Czy przy hotelu znajduje się parking?/Ile kosztuje parking?/ Gdzie znajdują się miejsca parkingowe dla gości. Miejsca parkingowe znajdują się bezpośrednio przy budynku hotelu. Parking dla naszych Gości jest bezpłatny

    Gdzie znajduje się hotel?

Hotel znajduje się w ….

    Jakie pokoje znajdują się w hotelu. W hotelu dostępne są pokoje dwuosobowe z 1 lub 2 łóżkami
    Od czego zależy cena pobytu?

Cena pobytu zależy od terminu i długości pobytu,rodzaju pokoju oraz wybranego wariantu wyżywienia. Parking dla gości oraz strefa saun dla gości są bezpłatne. W celu przygotowania oferty, prosimy o kontakt telefoniczny z recepcją (nr.tel.) lub wpisanie poniżej preferowanej daty pobytu, pakiety dostępne sa również na naszej stronie internetowej….

    Czy w hotelu są jakieś atrakcje?

W ramach pobytu można skorzystac ze strefy saun, solarium, sali fitness oraz biblioteki zlokalizowanej w hallu hotelu. Posiadamy również wiele gier planszowych, które można wypożyczyć do pokoju. W celu skorzystania z oferty SPA, zapraszamy do kontaktu z recepcją..

    Jakie są godziny otwarcia restauracji?

Godziny otwarcia restauracji poza sezonem letnim: • Poniedziałek – Sobota 13:00 – 21:00 • Niedziela 13:00 – 20:00 Godziny otwarcia baru: • Codziennie 8:00 – 24:00 Śniadania: • Codziennie 7:00-11:00

    Jakie są godziny wymeldowania i zameldowania?/ Od której mogę się zameldować?/ Do której musze się wymeldować?

Zameldowanie w obiekcie rozpoczyna się o 15:00, a wymeldować można się do 11:00.

