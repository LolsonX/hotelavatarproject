class CreateMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :messages do |t|
      t.integer :attitude
      t.string :content
      t.string :trait

      t.timestamps
    end
  end
end
