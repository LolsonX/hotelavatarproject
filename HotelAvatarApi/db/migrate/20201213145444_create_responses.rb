class CreateResponses < ActiveRecord::Migration[6.0]
  def change
    create_table :responses do |t|
      t.string :content
      t.string :trait
      t.string :intent
      t.string :entities
      t.string :attitude

      t.timestamps
    end
  end
end
