# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
[
  { content: 'Zameldowanie w obiekcie rozpoczyna się o 15:00, a wymeldować można się do 11:00.',
    trait: nil,
    intent: 'checkout',
    entities: nil,
    attitude: 'neutral' },

  { content: 'W ramach pobytu można skorzystac ze strefy saun, solarium, sali fitness oraz biblioteki zlokalizowanej w
              hallu hotelu. Posiadamy również wiele gier planszowych, które można wypożyczyć do pokoju.
              W celu skorzystania z oferty SPA, zapraszamy do kontaktu z recepcją.',
    trait: nil,
    intent: 'attractions',
    entities: nil,
    attitude: 'neutral' },

  { content: 'Informacja o jedzeniu',
    trait: nil,
    intent: 'food',
    entities: nil,
    attitude: 'neutral' },

  { content: 'Hotel znajduje się w miejscowości Mielno, ul. Bolesława Chrobrego 19. Poza dojazdem samochodem prywatnym,
              można zdecydować się na transport busem z Koszalina i wysiąść na przystanku Centrum. Kolejnym sposobem
              dotarcia jest przyjazd pociągiem, który kursuje w sezonie letnim. Po wyjsciu ze stacji wystarczy kierować
              się w lewo i trzymać się głównej drogi. Po około 500m będzie widać nasz ośrodek',
    trait: nil,
    intent: 'how_to_arrive',
    entities: nil,
    attitude: 'neutral' },

  { content: 'Aby zarezerwować pokój można skontaktować się z recepcją telefonicznie, odwiedzić naszą stronę internetową
              lub wpisać poniżej datę pobytu i liczbę osób (Dzieci powyżej 13 lat sa liczone jako osoby dorosłe)',
    trait: 'greeting',
    intent: 'how_to_reserve',
    entities: nil,
    attitude: 'neutral' },

  { content: 'Cena pobytu zależy od terminu i długości pobytu,rodzaju pokoju oraz wybranego wariantu wyżywienia.
              Parking dla gości oraz strefa saun dla gości są bezpłatne. W celu przygotowania oferty, prosimy o
              kontakt telefoniczny z recepcją (nr.tel.) lub wpisanie poniżej preferowanej daty pobytu,
              pakiety dostępne sa również na naszej stronie internetowej',
    trait: 'greeting',
    intent: 'price',
    entities: nil,
    attitude: 'neutral' },

  { content: 'Dziękujemy za zainteresowanie Naszą ofertą. Rezerwacja została zgłoszona na recepcje, szukamy już
              najlepszych wariantów. W czasie oczekiwania zachęcamy do zapoznania się z zasadami funkcjonowania
              hotelu.',
    trait: nil,
    intent: 'reservation',
    entities: 'num_of_kids, num_of_adults, datetime',
    attitude: 'positive' },

  { content: 'Dziękujemy za zainteresowanie Naszą ofertą. Rezerwacja została zgłoszona na recepcje, szukamy już
              najlepszych wariantów. W czasie oczekiwania zachęcamy do zapoznania się z zasadami funkcjonowania
              hotelu.',
    trait: nil,
    intent: 'reservation',
    entities: 'num_of_kids, num_of_adults, datetime',
    attitude: 'neutral' },

  { content: 'Wybrany termin jest niedostępny',
    trait: 'greeting',
    intent: 'reservation',
    entities: 'num_of_kids,num_of_adults',
    attitude: 'negative' },

  { content: 'Godziny otwarcia restauracji poza sezonem letnim: • Poniedziałek – Sobota 13:00 – 21:00 • Niedziela
              13:00 – 20:00 Godziny otwarcia baru: • Codziennie 8:00 – 24:00 Śniadania: • Codziennie 7:00-11:00',
    trait: nil,
    intent: 'restaurant_hours',
    entities: nil,
    attitude: 'neutral'
  },
  { content: 'W hotelu dostępne są pokoje dwuosobowe z 1 lub 2 łóżkami',
    trait: nil,
    intent: 'rooms',
    entities: nil,
    attitude: 'neutral'
  }
].each do |resp|
  Response.create resp
end
