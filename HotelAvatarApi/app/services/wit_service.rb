# frozen_string_literal: true

# Class used to connect and exchange information with WitAI API
class WitService < ApplicationService
  def call(**kwargs)
    client.message kwargs[:msg]
  end

  def client
    @client ||= Wit.new access_token: 'QS7MBSUCTLQUKS4UN2HCQDJJO22L2GYP'
  end

end
