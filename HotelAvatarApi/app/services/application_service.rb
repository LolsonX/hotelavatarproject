class ApplicationService
  def self.call(**kwargs, &block)
    new(**kwargs, &block).call(**kwargs)
  end
  def initialize(**kwargs, &block); end
end
