class MessagesController < ApplicationController
  def show
    wit_response = WitService.call(msg: message_params[:message])
    intent = wit_response['intents'].max {|intent| intent['confidence']}
    resp = Response.find_by intent: intent['name'] unless intent.nil?
    render json: resp.to_hash and return unless resp.nil?

    render json: {message: "Cannot find response"}, status: 404
  end

  private

  def message_params
    params.permit(:message)
  end
end
