# frozen_string_literal: true

class Response < ApplicationRecord
  def to_hash
    {
      content: ERB.new(content.squish).result,
      attitude: attitude,
      trait: trait,
      entities: entities,
      intent: intent,
      created_at: created_at,
      updated_at: updated_at
    }
  end
end
