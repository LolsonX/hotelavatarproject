import { AfterViewInit, Component, ElementRef, HostListener, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TweenLite, gsap } from 'gsap';
import * as gsapCore from 'gsap/gsap-core';
import { fromEvent } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { MessageService } from '../_services/message.service';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit, AfterViewInit {

  private eyesMover: ElementsMover;

  form: FormGroup;
  conversation: Message[] = [];

  @ViewChild('person')
  personRef: ElementRef;

  @ViewChild('desk')
  deskRef: ElementRef;

  @ViewChild('textarea')
  textareaRef: ElementRef;

  @ViewChild('mouth')
  mouthImgRef: ElementRef;

  @ViewChildren('pupil')
  pupils: QueryList<ElementRef>;

  attitudesImgs: { [key: string]: string; } = {
    'neutral': 'assets/img/neutral.svg',
    'happy': 'assets/img/happy.svg',
    'sad': 'assets/img/sad.svg',
  };

  @ViewChild('conversationHistory', { static: false })
  conversationHistoryRef: ElementRef;

  constructor(
    private fb: FormBuilder,
    private service: MessageService,
    private renderer: Renderer2
  ) { }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(e: MouseEvent): void {
    this.eyesMover.update(
      ...this.calcCoordsBasedOnMouse(e)
    );
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      question: this.fb.control(null, Validators.required)
    });
  }

  ngAfterViewInit(): void {
    this.conversation.push({
      content: 'Dzień dobry!',
      sender: 'server'
    });

    this.eyesMover = new ElementsMover(
      ...this.pupils.toArray().map(p => p.nativeElement)
    );

    TweenLite.from(this.personRef.nativeElement, 1, {
      opacity: 0,
      left: 0
    });

    fromEvent<KeyboardEvent>(this.textareaRef.nativeElement, 'keyup')
      .subscribe(() => {
        this.eyesMover.update(
          ...this.calcCoordsBasedOnTextArea()
        );
      });

    let awaitingAnimation: number;
    let inProgressAnimation: gsapCore.TweenLite;

    fromEvent<KeyboardEvent>(this.textareaRef.nativeElement, 'keydown')
      .pipe(
        filter((e) => {
          return e.key === 'Enter' && !!(this.form.get('question').value as string)?.trim();
        }),
        map((e) => {
          e.preventDefault();

          this.conversation.push({
            content: this.form.get('question').value,
            sender: 'user'
          });
          this.scrollToBottom();
          this.form.reset();

          return this.conversation.slice(-1)[0].content;
        }),
        switchMap((question: string) => {
          return this.service.sendQuestion(question);
        }),
        tap((resp) => {
          if (awaitingAnimation)
            clearTimeout(awaitingAnimation);

          if (inProgressAnimation)
            inProgressAnimation.totalProgress(1).kill();

          this.conversation.push({
            content: resp.content,
            sender: 'server'
          });
          this.scrollToBottom();
        })
      )
      .subscribe((resp) => {

        const setImg = (key: string): void => {
          this.renderer.setProperty(
            this.mouthImgRef.nativeElement,
            'src',
            this.attitudesImgs[key]
          );
        };

        const animate = (): void => {
          inProgressAnimation = gsap.from(this.mouthImgRef.nativeElement,  {
            opacity: 0
          });
        };

        setImg(resp.attitude);
        animate();

        awaitingAnimation = window.setTimeout(() => {
          setImg('neutral');
          animate();
        }, 3000);
      });
  }

  private calcCoordsBasedOnMouse(e: MouseEvent): [string, string] {
    const yOffset = 300;

    return [
      (e.clientX * 100 / window.innerWidth) + '%',
      ((e.clientY + yOffset) * 100 / window.innerHeight) + '%'
    ];
  }

  private calcCoordsBasedOnTextArea(): [string, string] {
    const lineCharsCount = 61;
    const textArea = this.textareaRef.nativeElement;

    const areaLeft = (window.innerWidth - textArea.clientWidth) / 2;
    const cursorOffset = ((textArea.selectionStart % lineCharsCount) / lineCharsCount) * textArea.clientWidth;

    return [
      ((areaLeft + cursorOffset) * 100 / window.innerWidth) + '%',
      ((window.innerHeight + 100) * 100 / window.innerHeight) + '%'
    ];
  }

  private scrollToBottom() {
    if (!this.conversationHistoryRef)
      return;

    setTimeout(() => {
      this.conversationHistoryRef.nativeElement.scrollTo({
        left: 0 ,
        top: this.conversationHistoryRef.nativeElement.scrollHeight,
        behavior: 'smooth'
      });
    });
  }
}

class ElementsMover {
  private elements: HTMLElement[];

  constructor(...elements: HTMLElement[]) {
    this.elements = [...elements];
  }

  update = (x: string, y: string): void => {
    this.elements.forEach(e => {
      e.style.left = x;
      e.style.top = y;
      e.style.transform = `translate(-${x}, -${y})`;
    });
  }
}

interface Message {
  sender: string;
  content: string;
}
