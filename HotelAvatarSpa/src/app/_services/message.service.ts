import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, delay, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private url = environment.url;

  constructor(
    private http: HttpClient
  ) { }

  sendQuestion(question: string): Observable<Response> {
    return this.http.post<Response>(`${this.url}messages/show`, { message: question })
      .pipe(
        // delay(500),
        map(res => {
          console.log(res);

          if (res.attitude === 'positive') {
            res.attitude = 'happy';
          }

          if (res == null) {
            return {
              attitude: 'sad',
              content: 'Nie potrafię odpowiedzieć na to pytanie.'
            };
          }
          return res;
        }),
        catchError((e) => {
          return of({
            attitude: 'sad',
            content: 'Nie potrafię odpowiedzieć na to pytanie.'
          });
        }),
      );
  }
}

interface Response {
  id?: number;
  content?: string;
  trait?: any;
  intent?: string;
  entities?: any;
  attitude?: string;
  created_at?: string;
  updated_at?: string;
}
