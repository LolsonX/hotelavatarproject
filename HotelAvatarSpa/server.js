const express = require('express');

const app = express();

app.use(express.static('./public'));

app.get('/*', function (_, res) {
  res.sendFile('index.html', { root: 'public' });
});

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Running on port ${PORT}`);
});
